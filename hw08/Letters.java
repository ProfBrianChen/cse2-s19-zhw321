//////////////
//// CSE 02 Letter
///
import java.util.Random;// import a random number generator 
public class Letters{
  public static void main(String args []){
    Random randomGenerator = new Random();
    char [] letter = new char [20]; //  allocate spaces for an array
    
    System.out.print("Random character array: ");
    
    for(int i = 0; i < letter.length; i++){
      int category = randomGenerator.nextInt(2); // generate a random number between 0 and 1
      int randomNum;
      if (category == 0){                        // two seperate categories
      randomNum = randomGenerator.nextInt(26) + 65; // condition 
    }
    else {
      randomNum = randomGenerator.nextInt(26) + 97; // condition
    }
   
      int x = randomNum;  // declare another variable
      letter[i] = (char) x; // cast numbers to letters by using ASCII
      System.out.print(letter[i]);
    }
    System.out.println();
    System.out.print("AtoM characters: ");
    char [] array2 = aToM(letter);
      for(int i = 0; i < array2.length; i++){
      System.out.print(array2[i]);
    }
    System.out.println();
    System.out.print("NtoZ characters: ");
    char [] array3 = nToZ(letter);
      for(int i = 0; i < array3.length; i++){
      System.out.print(array3[i]);
      }
    System.out.println();
  }
  //original = YDoGDoGYdOUDGY
  public static char [] aToM(char[] original) {
    char [] aToM = new char [original.length];
    int pos = 0;
    for(int i = 0; i < original.length; i++){
    int num = (int) original[i];                  // cast the letter back to number
      if((65 <= num && num <= 77) || (97 <= num && num <= 109)){
        aToM[pos] = original[i];
        pos++;
      } else {
        
      }
    }
    return aToM;
  }
  
  public static char [] nToZ(char[] original) {
    char [] nToZ = new char [original.length];
    int pos = 0;
    for(int i = 0; i < original.length; i++){
    int num = (int) original[i];                 // cast the letter back to number
      if((78 <= num && num <= 90) || (110 <= num && num <= 122)){
        nToZ[pos] = original[i];
        pos++;
      }
      
    }
    return nToZ;
  }
  
  
}