//////////////
//// CSE 02 PlayLottery
///
import java.util.Random; // import the random number generator 
import java.util.Scanner; // import the scanner 
public class PlayLottery{
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner(System.in);
    System.out.println("Please provide 5 numbers between 0 to 59: "); 
    int[] user = new int[5];      // declare the array
    user[0] = myScanner.nextInt();  // initialize the array
    user[1] = myScanner.nextInt();
    user[2] = myScanner.nextInt();
    user[3] = myScanner.nextInt();
    user[4] = myScanner.nextInt();
    
    int[] autoGenerate = numbersPicked(); //call the method of the random number picked 
    System.out.print("The winning numbers are: "); 
    for(int i = 0; i < autoGenerate.length; i++){   // print out the auto generated numbers 
      System.out.print(autoGenerate[i] + " ");
    }
    System.out.println();
    if (userWins(user, autoGenerate)){   // call the boolean method 
      System.out.println("You win!");   
    }
    else{
      System.out.println("You lose");
    }
    
  }
  
  public static int[] numbersPicked(){     // auto generate numbers 
    Random randomGenerator = new Random();
    int[] winning = new int [5];
    winning[0] = randomGenerator.nextInt(60);
    int x1 = randomGenerator.nextInt(60);
    while (x1 == winning[0]){               // to make sure there are no duplicated numbers 
        x1 = randomGenerator.nextInt(60);
    }
    winning[1] = x1;
    
    int x2 = randomGenerator.nextInt(60);
    while (x2 == winning[0] || x2 == winning[1]){
        x2 = randomGenerator.nextInt(60);
    }
    winning[2] = x2;
    
    int x3 = randomGenerator.nextInt(60);
    while (x3 == winning[0] || x3 == winning[1] || x3 == winning[2]){
        x3 = randomGenerator.nextInt(60);
    }
    winning[3] = x3;
    
    int x4 = randomGenerator.nextInt(60);
    while (x4 == winning[0] || x4 == winning[1] || x4 == winning[2] || x4 == winning[3]){
        x4 = randomGenerator.nextInt(60);
    }
    winning[4] = x4;
    
    return winning;                               // return an array
  }
  
  public static boolean userWins(int[] user, int[] winning){ 
    for(int i = 0; i < user.length; i++){
      boolean check = false;
      for(int j = 0; j < winning.length; j++){
        if(user[i] == winning[j]){
          check = true;
          break;
        }
      }
      if(!check){                       // return the boolean conditon 
        return false;
      }
    }
    return true;               
  }
}