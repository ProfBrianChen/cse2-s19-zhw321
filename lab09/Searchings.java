//////////////
//// CSE 02 Searchings
///
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;
public class Searchings{
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner (System.in);
    System.out.println("Would you like to perform a linear or binary search?");
    while(true){
      String answer = myScanner.next();
      if(answer.equals("linear")){
        System.out.println("Please give me an array size: ");
        int siz = myScanner.nextInt();
        System.out.println("Please give me an index to search for: ");
        int linindex = myScanner.nextInt();
        int[] arr1 = generateArray(siz);
       // linearSearching(arr1, linindex);
        System.out.println("The generated array is: ");
        for(int i = 0; i < arr1.length; i++){
          System.out.print(arr1[i] + " ");
        }
        System.out.println();
        System.out.println("The index is: " + linindex);
        System.out.println("The size is: " + siz);
        System.out.println("The term can be found at " + linearSearching(arr1, linindex) );
        break;
      }
      else if (answer.equals("binary")){
        System.out.println("Please give me an array size: ");
        int siz2 = myScanner.nextInt();
        System.out.println("Please give me an index to search for: ");
        int linindex2 = myScanner.nextInt();
        int[] arr2 = ascendingArray(siz2);
        System.out.println("The generated array is: ");
        for(int i = 0; i < arr2.length; i++){
          System.out.print(arr2[i] + " ");
        }
        System.out.println();
        System.out.println("The index is: " + linindex2);
        System.out.println("The size is: " + siz2);
        System.out.println("The term can be found at " + binarySearching(arr2, linindex2));
        
        break;
      }
      else { 
        System.out.println("Please type either binary or linear. ");
      }
    }
    //int[] aArray = new int[30];
  }
  public static int[] generateArray(int size){   
    Random randomGenerator = new Random();
    int[] aArray = new int[size];
    for(int i = 0; i < aArray.length; i++){
      aArray[i] = randomGenerator.nextInt(size + 1);
    }
    return aArray;
  }
  
  public static int[] ascendingArray(int size){
    Random randomGenerator = new Random();
    int[] aArray = new int[size];
    for (int i = 0; i < aArray.length; i++){
      aArray[i] = randomGenerator.nextInt(size) + 1;
      
    }
    Arrays.sort(aArray);
    return aArray;
  }
  public static int linearSearching(int[] random, int index){
    int notFound = -1;
    for ( int i = 0; i < random.length; i++){
      if ( index == random[i]){
        return i;
      }
    }
    return notFound;
  }
  public static int binarySearching(int[] random, int index){
    int notFound = -1;
    int low = 0;
    int high = random.length - 1;
    int mid = low + (high - low)/2;
    while(low < high){
      if(random[mid] > index){
        high = mid - 1;
      }
      else if(random[mid] < index){
        low = mid + 1;
      }
      else{
        return mid;
      }
      mid = low + (high - low)/2;
    }
    return notFound;
  }
} 