//////////////
//// CSE 02 BoxVolume
///
import java.util.Scanner;	//import
public class BoxVolume{
  
public static void main(String args[]){
  Scanner myScanner; //declare the scanner
  myScanner = new Scanner ( System.in ); // connect the scanner with the system
  System.out.print("The width side of the box is: "); // prompt the user to enter the width of the box
  double myNumber1 = myScanner.nextDouble();
  System.out.print("The length of the box is: "); // prompt the user to enter the length of the box
  double myNumber2 = myScanner.nextDouble();
  System.out.print("The height of the box is: "); // prompt the user to enter the height of the box
  double myNumber3 = myScanner.nextDouble();
  double answer = myNumber1 * myNumber2 * myNumber3; // perform the calculation
  System.out.println("The volume inside the box is: " + answer); // print out the volume of the box
  
    
}
}