//////////////
//// CSE 02 Convert
///
import java.util.Scanner;	//import
public class Convert{
  
public static void main(String args[]){
  Scanner myScanner; //declare the scanner
  myScanner = new Scanner ( System.in ); // connect the scanner with the system
  System.out.print("Enter the distance in meters: "); // prompt the user to enter the input meters
  double myNumber = myScanner.nextDouble();
  double answer = myNumber * 39.37; // perform the conversion
  System.out.println(myNumber + " meters is " + answer + " inches."); // prin out the converted outcome
  }
}