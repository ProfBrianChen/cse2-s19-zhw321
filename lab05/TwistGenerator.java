//////////////
//// CSE 02 TwistGenerator
///
import java.util.Scanner;// import sacnner
public class TwistGenerator{
  
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner (System.in); // connect the scanner with the system
    int num1;
        System.out.print("Enter a positive integer: "); // ask for the input
        while (true) {
            try { 
                num1 = Integer.parseInt(myScanner.nextLine());             // check if the input is an integer
                while(num1 <= 0){                                         // check if the number is positive
                      System.out.print("Try again: ");
                      num1 = Integer.parseInt(myScanner.nextLine());
                }
                break;
            } catch (NumberFormatException nfe) {
                System.out.print("Try again: ");
            }
        }
    
        // print the first line 
        int position = 1; // declare a new variable
          while(position <= num1){
               if(position % 3 == 1){
                 System.out.print("\\");
             
                }
              else if(position % 3 ==2){
                System.out.print(" ");
              
              }
              else{
                System.out.print("/");
            
              }
            position++;
          }
          System.out.println(); //move it to another line
    
      // print the second line
      int position2 = 1; // declare a new variable
          while(position2 <= num1){
               if(position2 % 3 == 1){
                 System.out.print(" ");
             
                }
              else if(position2 % 3 ==2){
                System.out.print("X");
              
              }
              else{
                System.out.print(" ");
            
              }
            position2++;
          }
          System.out.println(); //move it to another line
    
        // print the third line      
        int position3 = 1; // declare a new variable
          while(position3 <= num1){
               if(position3 % 3 == 1){     // print the first character of the line
                 System.out.print("/");
             
                }
              else if(position3 % 3 ==2){  // print the second character of the line
                System.out.print(" ");
              
              }
              else{                        // print the third character of the line
                System.out.print("\\");
            
              }
            position3++;  // increment the variable by one
          }
          System.out.println(); //move it to another line
  }
    
}