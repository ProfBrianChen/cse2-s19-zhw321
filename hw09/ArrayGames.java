//////////////
//// CSE 02 ArrayGames
///
import java.util.Arrays;
import java.util.Random;
import java.lang.Math;
import java.util.Scanner;
public class ArrayGames{
  public static void main(String args[]){
    Random randomGenerator = new Random(); 
    int[] myArray = new int[randomGenerator.nextInt(10) + 10];   // generate my random array
    for(int i = 0; i < myArray.length; i++){                      // assign values 
      myArray[i] = randomGenerator.nextInt(100);
    }
    System.out.print("Input 1: ");
    Print(myArray);
    System.out.println();
    Scanner myScanner;
    myScanner = new Scanner (System.in);
    System.out.println("Would you like to run insert or shorten?: ");   // ask the user which program to run
    while(true){
    String answer = myScanner.next();                           // create another variable to store the input
    if (answer.equals("insert")){
      int[] arr = Generate();
      System.out.print("Input 2: ");
      Print(arr);
      System.out.println();
      int[] arr2 = Insert(myArray, arr);
      System.out.print("output: ");
      Print(arr2);
      System.out.println();    
      break;
    }
    else if (answer.equals("shorten")){
      int randomIndex = randomGenerator.nextInt(10);                     // generate a random index
      System.out.println("Input 2: " + randomIndex);
      int[] arr3 = Shorten(myArray, randomIndex);
      System.out.print("Output: ");    
      Print(arr3);
      System.out.println();    
      break;
    }
    else{
      System.out.println("Your answer is not acceptable, please either insert or shorten: ");
    }
    }
  }
  
  public static int[] Generate(){
    Random randomGenerator = new Random(); // generate a random number 
    int size = randomGenerator.nextInt(11) + 10; // generate a number between 10--20
    int[] randomArray = new int[size];
    for(int i = 0; i < randomArray.length; i++){
      randomArray[i] = randomGenerator.nextInt(50);
    }
    return randomArray; // return the generated array   
  }
  
  public static void Print(int[] printArray){
    for(int i = 0; i < printArray.length; i++){
      System.out.print(printArray[i] + " ");
    }
  }
  
     public static int[] Insert(int[] firstArray, int[] secondArray){
      Random randomGenerator = new Random();
      int randomNum = (int) randomGenerator.nextInt(firstArray.length);
      int[] neoArray = new int[firstArray.length + secondArray.length];
      for(int i = 0; i < neoArray.length; i++){                           // combine the two arrays 
        if (i < randomNum){
          neoArray[i] = firstArray[i];
        }
        else if (i < randomNum + secondArray.length){
          neoArray[i] = secondArray[i - randomNum];
        } 
        else {
          neoArray[i] = firstArray[i - secondArray.length];
        }
      }
      return neoArray;                                        // return the combined array
    }
  public static int[] Shorten(int[] inputArray, int inputIndex){
    if(inputIndex > inputArray.length){
      return inputArray;
    }
    else{
      int[] neoArray = new int[inputArray.length - 1];   // create a new array 
      for(int i = 0; i < neoArray.length; i++){
        if(i < inputIndex){
          neoArray[i] = inputArray[i];
        }
        else{
          neoArray[i] = inputArray[i + 1];
        }
      }
      return neoArray;
    }
  }
}