//////////////
//// CSE 02 CardGenerator
///
public class CardGenerator{
  
  public static void main(String args[]){  
    int max = 52; // declare the maximum number of the card
    int min = 1; // declare the minimum number of the card
    int range = max - min + 1; // declare the card range
    int randomNumber = (int) (Math.random() * range) + min; // generate a random card
    int randomIdentity = randomNumber / 13;  // identify the suit of the card
    String cardSuit = "red"; // declare the card suit
    // find out the suit of the random generated card
    if (randomIdentity == 0) {
      cardSuit = "diamonds";
    } else if (randomIdentity == 1) {
      cardSuit = "clubs";
    } else if (randomIdentity == 2) {
      cardSuit = "hearts";
    } else {
      cardSuit = "spades";
    }
    int remainder = randomNumber % 13;
    String number = "ins"; // declare the number  
    // find out the number of the random generated card
    if (remainder == 1) {
      number = "ace";
    } else if (remainder == 2) {
      number = "2";
    } else if (remainder == 3) {
      number = "3";
    } else if (remainder == 4) {
      number = "4";
    } else if (remainder == 5) {
      number = "5";
    } else if (remainder == 6) {
      number = "6";
    } else if (remainder == 7) {
      number = "7";
    } else if (remainder == 8) {
      number = "8";
    } else if (remainder == 9) {
      number = "9";
    } else if (remainder == 10) {
      number = "10";
    } else if (remainder ==11) {
      number = "jack";
    } else if (remainder == 12) {
      number = "queen";
    } else {
      number = "king";
    }
    System.out.println("Your random cards were");
    System.out.println("    the " + number + " of " + cardSuit); // print out the statement
    
    
    

   
  }
  }