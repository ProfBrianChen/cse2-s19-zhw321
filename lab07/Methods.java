//////////////
//// CSE 02 Methods
///
import java.util.Random;// import a random number generator 
import java.util.Scanner; // import a scanner
public class Methods{
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner(System.in);
    
    //String noun = Thesis();
    //Action(noun);
    //Conclusion(noun);
    Final();
    
    while(true){
      System.out.println("Would you like to have another sentence? "); // query the user
       String val = myScanner.next(); // declare a new variable
      if (val.equals("yes")){
        //noun = Thesis();
        //Action(noun);
        //Conclusion(noun);
        Final();
      }
      else{
        break;
      }
    }
    //Final();
  }
  
  public static void Final(){
    String noun = Thesis();
    Random randomGenerator = new Random();
    int randomNum = randomGenerator.nextInt(10);
    for (int i = 0; i < randomNum; i++){
      Action(noun);
    }
    Conclusion(noun);
  }
  
  public static String Thesis(){
    String noun = Nouns();
    System.out.println("The " + Adjectives() + " " + Adjectives() + " " + noun + " " + Pasttense() + " the " + Adjectives() + " " + Objects() + ".");
    return noun;
  }
  
  public static void Action(String noun){
    Random randomGenerator = new Random();
    int random = randomGenerator.nextInt(100);
    if(random > 50){
      System.out.println("The " + noun + " was " + Adjectives() +" to " + Pasttense() + " " + Objects() + " .");
    }
    else{
      System.out.println("It used " + Objects() +" to " + Pasttense() + " " + Objects() +" at the " + Adjectives() + " " + Nouns() + ".");
    }
  }
  
  public static void Conclusion(String noun){
    System.out.println("That " + noun + " " + Pasttense() + " her " + Objects() + ".");
  }
  
  public static String Adjectives(){
    Random randomGenerator = new Random(); // connect the random generator 
    int randomInt = randomGenerator.nextInt(10); // generate an integer from 0--9
    String adj = "0";
    switch ( randomInt ){
      case 0:
        adj = "beautiful";
        break;
      case 1:
         adj = "big";
        break;
      case 2:
         adj = "brave";
        break;
      case 3:
       adj = "handsome";
        break;
      case 4:
        adj = "smart";
        break;
      case 5:
        adj = "scary";
        break;
      case 6:
        adj = "ridiculous";
        break;
      case 7:
       adj = "slow";
        break;
      case 8:
       adj = "hot";
        break;
      case 9:
        adj = "interesting";
        break;  
    }
    return adj; // return the variable
    
  }
  
  public static String Nouns(){
    Random randomGenerator = new Random(); // connect the random generator 
    int randomInt = randomGenerator.nextInt(10); // generate an integer from 0--9
    String noun = "0";
    switch ( randomInt ){
      case 0:
        noun = "dog";
        break;
      case 1:
         noun = "student";
        break;
      case 2:
         noun = "professor";
        break;
      case 3:
       noun = "deer";
        break;
      case 4:
        noun = "owl";
        break;
      case 5:
        noun = "soldier";
        break;
      case 6:
        noun = "policeman";
        break;
      case 7:
       noun = "fratbrother";
        break;
      case 8:
       noun = "athlete";
        break;
      case 9:
        noun = "dancer";
        break;  
    }
    return noun; // return the variable
    
  }
  public static String Pasttense(){
    Random randomGenerator = new Random(); // connect the random generator 
    int randomInt = randomGenerator.nextInt(10); // generate an integer from 0--9
    String tense = "0";
    switch ( randomInt ){
      case 0:
        tense = "took";
        break;
      case 1:
         tense = "went";
        break;
      case 2:
         tense = "broke";
        break;
      case 3:
       tense = "forgot";
        break;
      case 4:
        tense = "discovered";
        break;
      case 5:
        tense = "reminded";
        break;
      case 6:
        tense = "spilled";
        break;
      case 7:
       tense = "drank";
        break;
      case 8:
       tense = "ran";
        break;
      case 9:
        tense = "shot";
        break;  
    }
    return tense; // return the variable
    
  }
  
  public static String Objects(){
    Random randomGenerator = new Random(); // connect the random generator 
    int randomInt = randomGenerator.nextInt(10); // generate an integer from 0--9
    String sub = "0";
    switch ( randomInt ){
      case 0:
        sub = "banana";
        break;
      case 1:
         sub = "vodka";
        break;
      case 2:
         sub = "cellphone";
        break;
      case 3:
       sub = "fighter plane";
        break;
      case 4:
        sub = "submarine";
        break;
      case 5:
        sub = "lamp";
        break;
      case 6:
        sub = "sportscar";
        break;
      case 7:
       sub = "laptop";
        break;
      case 8:
       sub = "scooter";
        break;
      case 9:
        sub = "hawksburger";
        break;  
    }
    return sub; // return the variable
    
  }
}
