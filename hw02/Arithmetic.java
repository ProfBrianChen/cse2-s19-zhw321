//////////////
//// CSE 02 Arithmetic
///
public class Arithmetic{
  
  public static void main(String args[]){
 //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;
//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;
//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;
//the tax rate
double paSalesTax = 0.06;

    //calculation
    double totalCostOfPants; // declare a variable
    totalCostOfPants = numPants * pantsPrice; // calculation
    double totalCostOfShirts; //declare the variable
    totalCostOfShirts = numShirts * shirtPrice;// calculate the cost of shirts
    double totalCostOfBelts; // declare a variable
    totalCostOfBelts = numBelts * beltCost; // calculate the cost of belts
    double taxOfPants; // declare a new variable
    taxOfPants = paSalesTax * pantsPrice; // calculate the taxOfPants
    double taxOfShirts; // declare a variable
    taxOfShirts = paSalesTax * shirtPrice; // calculate the taxOfShirts
    double taxOfBelt; // declare a variable
    taxOfBelt = paSalesTax * beltCost; // calculate the beltCost
    
    //total cost of purchase
    double totalCostOfPurchase; // declare another variable
    totalCostOfPurchase = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;
    
    // total sales tax 
    double totalSalesTax; // declare the variable
    totalSalesTax = paSalesTax * totalCostOfPurchase; // calculate the totalSalesTax
    
    //total money paid
    double totalMoneyPaid; // declare the variable
    totalMoneyPaid = totalSalesTax + totalCostOfPurchase; // calculate the totalMoneyPaid
    
    
    //print the message
    System.out.println("The total cost of shirts is " + (int)(totalCostOfShirts * 100) / 100.0); //print out the cost of shirts
    System.out.println("The total cost of pants is " + (int)(totalCostOfPants * 100) / 100.0);  // print out the cost of pants
    System.out.println("The total cost of belts is " + (int)(totalCostOfBelts * 100) / 100.0);  // print out the cost of belts
    System.out.println("The sales tax of pants is " + (int)(taxOfPants * 100) / 100.0); // print out the tax
    System.out.println("The sales tax of shirts is " + (int)(taxOfShirts * 100) / 100.0); // print out the tax
    System.out.println("The sales tax of belts is " + (int)(taxOfBelt * 100) / 100.0); // print out the tax
    System.out.println("The total cost of purchase " + (int)(totalCostOfPurchase * 100) / 100.0); // print out the cost of purchase
    System.out.println("The total sales tax " + (int)(totalSalesTax * 100) / 100.0); // print out the total sales tax
    System.out.println("The total money paid " + (int)(totalMoneyPaid * 100) / 100.0); // print out the total money paid
  }
}