// cyclometer 
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

	  //end of main method   
 
int secsTrip1=480;  // time elapsed in seconds for first trip
int secsTrip2=3220;  // time elapsed in seconds for the second trip
int countsTrip1=1561;  // the number of rotations of the front wheel for trip one 
int countsTrip2=9037; // the number if rotations of the front wheel for trip two
double wheelDiameter=27.0,  // the diameter of the wheel
  	PI=3.14159, // the variable pi
  	feetPerMile=5280,  // unit conversion
  	inchesPerFoot=12,   // unit conversion
  	secondsPerMinute=60;  // unit conversion
	double distanceTrip1, distanceTrip2,totalDistance;  // introduce variables
 System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
 System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
// print out the variables that are stored and convert the units
distanceTrip1=countsTrip1*wheelDiameter*PI;
// compute the distance of trip1
distanceTrip1=inchesPerFoot*feetPerMile; // convert foot to miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;// compute the distance of trip 2 in miles
totalDistance=distanceTrip1+distanceTrip2;// the total distance in miles
// print out the data we want for trip 1, 2 and total trip on the terminal
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");
    } // end of main method
} // end of class