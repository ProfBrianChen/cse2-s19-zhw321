//////////////
//// CSE 02 Straight
///
import java.lang.Math;
import java.util.Scanner;
import java.util.Arrays;
public class Straight{
  public static void main(String args[]){
    int count = 0;
    for(int i = 0; i < 1000000; i++){
     int[] arr1 = Shuffle();
     int[] arr2 = Draw(arr1);
     int first = Integer.parseInt(Search(arr2, 1));
     int second = Integer.parseInt(Search(arr2, 2));     
     int third = Integer.parseInt(Search(arr2, 3));
     int forth = Integer.parseInt(Search(arr2, 4));
     int fifth = Integer.parseInt(Search(arr2, 5));
    
     if(second - first == 1 && third - second == 1 && forth - third == 1 && fifth - forth == 1 ){
       count = count + 1;
     }
   }
    System.out.println(count);
  // double result = (double) count / 1000000;
   //System.out.println("the probability is: " + result);
  }
  public static int[] Shuffle(){
    int[] shuffledC = new int [52];
    for(int j = 0; j < shuffledC.length; j++){
      shuffledC[j] = j + 1;
    }
    for(int i = 0; i < shuffledC.length; i++){
      int target = (int) (Math.random() * shuffledC.length);
      int temp = shuffledC[target];
      shuffledC[target] = shuffledC[i];
      shuffledC[i] = temp;
    }
    return shuffledC;
  }  
  public static int[] Draw(int[] target){
    int[] neoDraw = new int [5];
    for(int i = 0; i < 5; i++){
      neoDraw[i] = target[i]; 
    }
    return neoDraw;
  }
  public static String Search(int[] target, int k){
    if( k > 5 || k < 0){
      return "Error";
    }
    Arrays.sort(target);
    return "" + target[k-1];
  }
}