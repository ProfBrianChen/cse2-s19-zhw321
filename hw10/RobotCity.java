//////////////
//// CSE 02 RobotCity
///
import java.util.Random;
public class RobotCity{
  
  public static void main(String args[]){
    int[][] arr1 = buildCity();
    display(arr1);                       // calling each method 
    System.out.println();
    System.out.println();
    invade(arr1, 8);
    display(arr1);
    System.out.println();
    System.out.println();
    update(arr1);
    display(arr1);

    
  }
  
  public static int[][] buildCity(){
    Random randomGenerator = new Random();
    int [][] cityArray = new int [10 + randomGenerator.nextInt(6)][10 + randomGenerator.nextInt(6)];
    for(int i = 0; i < cityArray.length; i++){
      for(int j = 0; j < cityArray[i].length; j++){
        cityArray[i][j] = 100 + randomGenerator.nextInt(900);
      }
    }
    return cityArray;
  }
  public static void display(int[][] printArray){
 
    for(int i = 0; i < printArray.length; i++) {
      for(int j = 0;  j< printArray[0].length; j++) {
        System.out.printf("%5s", printArray[i][j]);
      }
      System.out.println();
    }
    
    // int max = 0;
    //for(int i = 0; i < printArray.length; i++){
      //if( printArray[i].length > max){
        //max = printArray[i].length;
      //}
    //}
    //for(int j = 0; j < max; j++){
      //for(int z = 0; z < printArray.length; z++){
        //if(printArray[z].length >= j+1){
          //System.out.print(printArray[z][j] + " ");
        //}
        //else{
          //System.out.print("   ");
        //}
      //}
      //System.out.println("");
    //}
  }
  public static int[][] invade(int[][] cityBlock, int k){
    Random randomGenerator = new Random();
    for(int z = 0; z < k; z++){
      int i = randomGenerator.nextInt(cityBlock.length);
      int j = randomGenerator.nextInt(cityBlock[0].length);
      while(cityBlock[i][j] >= 0){
        cityBlock[i][j] = - cityBlock[i][j];
      } 
    }
    return cityBlock;
  }
  public static int[][] update(int[][] updateCity){
    for(int i = 0; i < updateCity.length; i++){
      for(int j = updateCity[i].length - 1; j >= 0; j--){
        if(updateCity[i][j] < 0){
          updateCity[i][j] = - updateCity[i][j];
          if(j != updateCity[i].length - 1){
            updateCity[i][j + 1] = - updateCity[i][j + 1];
          }
        }
      }
    }
    return updateCity;
  }
}