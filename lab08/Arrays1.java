//////////////
//// CSE 02 Arrays
///
import java.util.Arrays;
import java.util.Random;
import java.lang.Math;
public class Arrays1 {
  public static void main (String args[]){
    int[] randomArray = new int[68];
    Random randomGenerator = new Random();
    for(int i = 0; i < randomArray.length; i++){
     randomArray[i] = randomGenerator.nextInt(100);
    }
    System.out.println("The range of the array is: " + getRange(randomArray));
    System.out.println("The mean of the array is: " + getMean(randomArray));
    System.out.println("The standard deviation of the array is: " + getStdDev(randomArray));
    shuffle(randomArray);
    System.out.println("The shuffled array is: ");
    for(int i = 0; i < randomArray.length; i++){
      System.out.print(randomArray[i] + " ");
    }
    System.out.println();
  }

  public static int getRange(int[] random){
    Arrays.sort(random);
    int range = random[random.length - 1] - random[0];
    return range;
  }
  
  public static double getMean(int[] random){
    int arraySum = 0;
    for(int i = 0; i < random.length; i++){
       arraySum = arraySum + random[i];
    }
    double mean = arraySum / (double)random.length ;
    return mean;
  }
  
  public static double getStdDev(int[] random){
    double standD = 0;
    double variance = 0;
    double sum = 0;
    for(int i = 0; i < random.length; i++){
      sum =  sum + (random[i] - getMean(random) ) * (random[i] - getMean(random) );
    }
    variance = sum / (random.length - 1);
    standD = Math.sqrt(variance);
    return standD;
  }
  
  public static void shuffle (int[] random){
     Random randomGenerator = new Random();
   for (int i = 0; i < 67; i++){
      int x1 = randomGenerator.nextInt(random.length);
      int x2 = randomGenerator.nextInt(random.length);
     while (x2 == x1){
         x2 = randomGenerator.nextInt(random.length);
     }
    
      int temp = random[x1];
      random[x1] = random[x2];
      random[x2] = temp;
   }
  }
}