//////////////
//// CSE 02 Area
///
import java.lang.Math.*; // import the math function
import java.util.Scanner; // import the scanner 
public class Area{
  public static void main (String args[]){
    Scanner myScanner;  // declare myScanner
    myScanner = new Scanner (System.in); // connect the scanner with the system
    System.out.println("Please enter the shape: ");
    while (myScanner.hasNext()){
      String shape = myScanner.next();
      if(shape.equals("rectangle")){
        System.out.println("Please enter the height: "); // prompt the user to enter the height 
        double height = 0;
        double width = 0;
        
        while(true){
          if(myScanner.hasNextDouble()){
            height = myScanner.nextDouble();
            if(height > 0){
              break;
            } else {
              System.out.println("Invalid input. Please enter the height: "); // prompt the user to enter the new height 
            }
          } else {
            String junk = myScanner.next();
            System.out.println("Invalid input. Please enter the height: "); // prompt the user to enter the new height 
          }
        } 
        System.out.println("Please enter the width: "); // prompt the user to enter the width 
        
         while(true){
          if(myScanner.hasNextDouble()){
            width = myScanner.nextDouble();
            if(width > 0){
              break;
            } else {
              System.out.println("Invalid input. Please enter the width: "); // prompt the user to enter the new height 
          
            }
          } else {
            String junk = myScanner.next();
            System.out.println("Invalid input. Please enter the width: "); // prompt the user to enter the new width 
          }
        }
        System.out.println("The area is " + rectArea(height, width));
        break;
      } else if (shape.equals("triangle")){
        System.out.println("Please enter the base: "); // prompt the user to enter the base 
        double base = 0;
        double triHeight = 0;
        
       while(true){
          if(myScanner.hasNextDouble()){
            base = myScanner.nextDouble();
            if(base > 0){
              break;
            } else {
              System.out.println("Invalid input. Please enter the base: "); // prompt the user to enter the new height 
          
            }
          } else {
            String junk = myScanner.next();
            System.out.println("Invalid input. Please enter the base: "); // prompt the user to enter the new base
          }
        }
        System.out.println("Please enter the triHeight: "); // prompt the user to enter the triHeight 
         
        while(true){
          if(myScanner.hasNextDouble()){
            triHeight = myScanner.nextDouble();
            if(triHeight > 0){
              break;
            } else {
              System.out.println("Invalid input. Please enter the triHeight: "); // prompt the user to enter the new height 
          
            }
          } else {
            String junk = myScanner.next();
            System.out.println("Invalid input. Please enter the triHeight: "); // prompt the user to enter the new triHeight
          }
        }
        System.out.println("The area is " + triArea(base, triHeight));
        
        break;
      } else if (shape.equals("circle")){
        System.out.println("Please enter the radius: "); // prompt the user to enter the radius 
        double radius = 0;
       while(true){
          if(myScanner.hasNextDouble()){
            radius = myScanner.nextDouble();
            if(radius > 0){
              break;
            } else {
              System.out.println("Invalid input. Please enter the radius: "); // prompt the user to enter the new height 
          
            }
          } else {
            String junk = myScanner.next();
            System.out.println("Invalid input. Please enter the radius: "); // prompt the user to enter the new radius
          }
        }
        System.out.println("The area is " + cirArea(radius));
        break;
      } else {
        
      }
    }
  }
  public static double rectArea (double height, double width){
    double rectArea = height * width;
    return rectArea;
  }
  
  public static double triArea (double base, double triHeight){
    double triArea = 0.5 * base * triHeight;
    return triArea;
  } 
  
  public static double cirArea (double radius){
   double cirArea = radius * radius * Math.PI;
    return cirArea;
 }
}