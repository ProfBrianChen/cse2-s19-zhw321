//////////////
//// CSE 02 StringAnalysis
///
import java.util.Scanner;
public class StringAnalysis{
  public static void main (String args[]){
    Scanner myScanner;  // declare myScanner
    myScanner = new Scanner (System.in); // connect the scanner with the system
    System.out.println("Please provide a string: ");
    String inputS = myScanner.next();  // store the input as a variable
    System.out.println("Please choose if you want to examine all the characters or a number of them by entering all or some "); 
    while(true){                               
      String answer = myScanner.next(); // store the input as a variable
      if(answer.equals("all")){
        System.out.println(stringAnalysis(inputS));                       // call the first method, and done
        break; // break out of the while loop
      } 
      else if(answer.equals("some")) {
        System.out.println("How many characters do you want to examine? ");
        while(true) {
          if (myScanner.hasNextInt()){                                         // check if the input is an integer
            int number = myScanner.nextInt();
             if(number > 0 ){
              System.out.println(stringAnalysis(inputS, number));               // call the second method, method overload 
              break;
             }
            else{
              System.out.println("Invalid answer, you must answer a positive integer ");
            }
          } else {
            System.out.println("Invalid answer, you must answer a positive integer ");
            String junk = myScanner.next(); // keep it as a junk 
          }
        }
        break; // important break which breaks out of the outer loop
      }
       else {
         System.out.println("Answer is invalid, you must answer either all or some ");
        
       }
     }
  }
  // first method
  public static boolean stringAnalysis(String siJun){
    for (int i = 0; i < siJun.length(); i++){
      if(siJun.charAt(i) <= 'z' && 'a' <= siJun.charAt(i) ){
        
      }
      else {
          return false; 
      }
    }
    return true;
  }
  
  // method overload
  public static boolean stringAnalysis(String ssiJun, int num){
    if(num <= ssiJun.length()){
      for (int i = 0; i < num; i++){
        if(ssiJun.charAt(i) <= 'z' && 'a' <= ssiJun.charAt(i) ){
        
        }
        else {
            return false; 
        }
      }
      return true;
    }
    else{
        for (int i = 0; i < ssiJun.length(); i++){
        if(ssiJun.charAt(i) <= 'z' && 'a' <= ssiJun.charAt(i) ){
        
        }
        else {
            return false; 
        }
      }
      return true;
    }
  }
}