//////////////
//// CSE 02 PatternC
///
import java.util.Scanner; // import the scanner
public class PatternC{
  
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner (System.in); // connect the scanner to the system
    System.out.println("Please provide an integer within the range of 1--10: "); // prompt the user to provide an integer
    
    int myNumber;
    
    while(true){
     if(!myScanner.hasNextInt()){
      System.out.println("Error! Please enter an integer within the range of 1--10: "); // error message
      String iPut = myScanner.next();
     } else {
        myNumber = myScanner.nextInt(); // declare another variable
      if(myNumber > 10 || myNumber < 1){
        System.out.println("Error! Please enter an integer within the range of 1--10: "); // error message
      } else {
        break;
      }
     } 
    }
    
    for(int i = 1; i<=myNumber; i++){
      for(int j = 1; j<=12; j++){
      if(j<= 12-i && j>=1){
        System.out.print(" ");
      }
      else{ 
        System.out.print(13-j);
      }
      }
      System.out.println();
    }
}
}