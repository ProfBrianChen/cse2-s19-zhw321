//////////////
//// CSE 02 PatternD
///
import java.util.Scanner; // import the scanner
public class PatternD{
  
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner (System.in); // connect the scanner to the system
    System.out.println("Please provide an integer within the range of 1--10: "); // prompt the user to provide an integer
    
    int myNumber;
    
    while(true){
     if(!myScanner.hasNextInt()){
      System.out.println("Error! Please enter an integer within the range of 1--10: "); // error message
      String iPut = myScanner.next();
     } else {
        myNumber = myScanner.nextInt(); // declare another variable
      if(myNumber > 10 || myNumber < 1){
        System.out.println("Error! Please enter an integer within the range of 1--10: "); // error message
      } else {
        break;
      }
     } 
    }
    for(int i = myNumber; i>=1; i--){
      for(int j = i; j>=1;j--){
        System.out.print(j + " "); 
      }   
      System.out.println();
    }
  }
}