//////////////
//// CSE 02 PatternA
///
import java.util.Scanner; // import the scanner
public class PatternA{
  
  public static void main(String args[]){
    Scanner myScanner;
    myScanner = new Scanner (System.in); // connect the scanner to the system
    System.out.println("Please provide an integer within the range of 1--10: "); // prompt the user to provide an integer
    
    int myNumber;
    
    while(true){
     if(!myScanner.hasNextInt()){
      System.out.println("Error! Please enter an integer within the range of 1--10: "); // error message
      String iPut = myScanner.next();
     } else {
        myNumber = myScanner.nextInt(); // declare another variable
      if(myNumber > 10 || myNumber < 1){
        System.out.println("Error! Please enter an integer within the range of 1--10: "); // error message
      } else {
        break;
      }
     } 
    }
    
    String line = ""; // declare a string variable
    for(int i = 1; i <= myNumber; i++){
      line = line + i + " ";
      System.out.println(line);
    
    
  }
}
}